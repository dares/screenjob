# Packages ----------------------------------------------------------------

library(data.table)
library(janitor)
library(shiny)
library(htmltools)
library(scales)
library(apexcharter)



# Fonctions ---------------------------------------------------------------

invisible(lapply(
  X = list.files(path = "funs", full.names = TRUE),
  FUN = source, encoding = "UTF-8"
))



# Datas -------------------------------------------------------------------

fap_data <- lecture_fap("inputs/")

fap1 <- filtre_fap(fap_data, c("A0Z"))
fap2 <- filtre_fap(fap_data, c("V4Z"))
ensemble <- filtre_fap(fap_data, c("Ensemble"))
ensemble <- ensemble$salaires




# Defaut ------------------------------------------------------------------

dat <- copy(fap1$salaires)

ordonne_salaire(dat)
ajout_libelle(dat)
graph_barres(
  data = dat,
  x = salaire,
  y = pourcentage / 100,
  couleurs = c("#113263", "#8e9594"), 
  verticales = FALSE
) %>% 
  ax_dataLabels(
    enabled = TRUE, 
    formatter = format_num(".0%")
  ) %>% 
  ax_xaxis(
    categories = wrap_labels(dat$salaire)
  )




# Basique -----------------------------------------------------------------

dat <- rbind(
  fap1$salaires,
  fap2$salaires
)
ordonne_salaire(dat)
ajout_libelle(dat)
graph_barres(
  data = dat,
  x = salaire,
  y = pourcentage / 100, 
  group = fap,
  couleurs = c("#113263", "#8e9594"), 
  verticales = FALSE
) %>% 
  ax_dataLabels(
    enabled = TRUE, 
    formatter = format_num(".0%")
  ) %>% 
  ax_xaxis(
    categories = wrap_labels(dat$salaire)
  )




# Tests -------------------------------------------------------------------


dat <- rbind(
  fap1$salaires,
  fap2$salaires
)
ordonne_salaire(dat)
ajout_libelle(dat)

dat[fap == "V4Z", pourcentage := -1 * pourcentage]

graph_barres(
  data = dat,
  x = salaire,
  y = pourcentage / 100, 
  group = fap,
  couleurs = c("#113263", "#8e9594"), 
  verticales = FALSE
) %>% 
  ax_chart(stacked = TRUE) %>% 
  ax_xaxis(min = -0.5) %>% 
  ax_yaxis(labels = list(show = FALSE)) %>% 
  ax_dataLabels(
    enabled = TRUE,
    enabledOnSeries = list(1),
    formatter = JS("function (val, opt) {\nreturn opt.w.globals.labels[opt.dataPointIndex];\n}"),
    textAnchor = "start", style = list(colors = list("#000"))
  )



graph_barres_divergentes(
  data = dat,
  x = salaire,
  y = pourcentage / 100, 
  group = fap,
  couleurs = c("#113263", "#8e9594")
)




graph_barres(
  data = dat,
  x = salaire,
  y = pourcentage / 100, 
  group = fap,
  couleurs = c("#113263", "#8e9594"), 
  verticales = FALSE
) %>% 
  ax_chart(stacked = TRUE) %>% 
  ax_xaxis(
    min = -0.5,
    labels = list(
      formatter = JS("function(value) {\n return d3.format('.0%')(Math.abs(value)); \n}")
    )
  ) %>%
  ax_tooltip(
    shared = TRUE, 
    y = list(
      formatter = JS("function(value) {\n return d3.format('.0%')(Math.abs(value)); \n}")
    )
  )

graph_barres(
  data = dat,
  x = salaire,
  y = pourcentage / 100, 
  group = fap,
  couleurs = c("#113263", "#8e9594"), 
  verticales = FALSE
)
