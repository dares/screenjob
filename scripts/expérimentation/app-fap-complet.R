
#  ------------------------------------------------------------------------
#
# Title : Exploration donnees completes
#    By : Victor
#  Date : 2020-02-26
#
#  ------------------------------------------------------------------------



# Packages ----------------------------------------------------------------

library(shiny)
library(readxl)
library(data.table)
library(DT)
library(apexcharter)
library(rlang)

source("../funs/graphiques.R")
source("../funs/couleurs.R")



# Datas -------------------------------------------------------------------

fap_data_complet <- readRDS("../datas/fap_data_complet.rds")



# App ---------------------------------------------------------------------


ui <- fluidPage(
  tags$h2("Exploration données complètes"),
  fluidRow(
    column(
      width = 3,
      selectizeInput(
        inputId = "fap", 
        label = "FAP:", 
        choices = unique(fap_data_complet$fap),
        width = "100%"
      ),
      selectizeInput(
        inputId = "fichier", 
        label = "Fichier Excel:", 
        choices = unique(fap_data_complet$fichier),
        width = "100%"
      ),
      selectizeInput(
        inputId = "feuille", 
        label = "Feuille du classeur:", 
        choices = unique(fap_data_complet$feuille),
        width = "100%"
      )
    ),
    column(
      width = 9,
      tabsetPanel(
        tabPanel(
          title = "Graphique",
          radioButtons(
            inputId = "type_courbe", 
            label = "Type :",
            choices = c("Courbes" = "c", "Aires empilées" = "a"),
            inline = TRUE
          ),
          apexchartOutput(outputId = "graph", height = "600px")
        ),
        tabPanel(
          title = "Tableau",
          DT::DTOutput(outputId = "tableau")
        )
      )
    )
  )
)

server <- function(input, output, session) {
  
  observeEvent(input$fichier, {
    feuilles <- fap_data_complet[fichier == input$fichier, unique(feuille)]
    updateSelectizeInput(session, "feuille", choices = feuilles)
  })
  
  
  fap_r <- reactive({
    fap_data_complet[fichier == input$fichier &
                       feuille == input$feuille &
                       fap == input$fap]
  })
  
  output$tableau <- DT::renderDT({
    fap_r()
  })
  
  output$graph <- renderApexchart({
    req(fap_r())
    dat <- copy(fap_r())
    dat[is.na(indicateur_label), indicateur_label := "-"]
    
    if (identical(input$type_courbe, "a")) {
      g <- graph_courbes(
        data = dat, 
        x = annees, y = valeur / 100, group = indicateur_label,  aire = TRUE,
        couleurs = substr(scales::viridis_pal()(uniqueN(dat$indicateur_label)), 1, 7), 
        format_y = ",", epaisseur = 3
      ) %>% 
        ax_chart(stacked = TRUE) %>% 
        ax_yaxis(max = 1) %>%
        # ax_xaxis(type = "numeric") %>% 
        ax_fill(opacity = 1, type = "solid")
    } else {
      g <- graph_courbes(
        data = dat, 
        x = annees, y = valeur, group = indicateur_label, 
        couleurs = substr(scales::viridis_pal()(uniqueN(dat$indicateur_label)), 1, 7), 
        format_y = ",", epaisseur = 3
      )
    }
    
    g <- g %>% 
      ax_chart(animations = list(enabled = FALSE))
    
    g
  })
  
}

shinyApp(ui, server)



