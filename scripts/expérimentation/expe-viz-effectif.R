# Packages ----------------------------------------------------------------

library(data.table)
library(janitor)
library(apexcharter)



# Fonctions ---------------------------------------------------------------

invisible(lapply(
  X = list.files(path = "funs", full.names = TRUE),
  FUN = source, encoding = "UTF-8"
))



# Datas -------------------------------------------------------------------

effectifs <- lecture_effectif("inputs/effectifs.csv")
effectifs

filtre_fap(effectifs, "A3Z")



# Viz ---------------------------------------------------------------------

apex(
  data = filtre_fap(effectifs, "T4Z"),
  mapping = aes(x = as.Date(paste0(annee, "-01-01")), y = effectif),
  type = "line"
) %>% 
  ax_yaxis(title = list(text = "en milliers")) %>% 
  ax_tooltip(
    x = list(
      format = "yyyy"
    ), 
    y = list(
      formatter = JS("function(val) {return val.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, \" \") + ' 000';}"),
      title = list(
        formatter = JS("function(val) {return 'Effectif ';}")
      )
    )
  ) %>% 
  ax_title(
    text = paste("Effectifs", obtenir_libelle("T4Z"), sep = " : ")
  ) %>% 
  ax_subtitle(
    text = "Source : enquêtes Emploi, Insee, données lissées par moyenne mobile d'ordre 3, traitement Dares.",
    style = list(
      fontSize = "11px"
    )
  )


graphique_effectifs(effectifs, "T4Z")

graphique_effectifs(filtre_fap(effectifs, "T4Z"))

graphique_effectifs(effectifs, "A1Z")


graphique_effectifs(effectifs, "B7Z")
graphique_effectifs(filtre_fap(effectifs, c("T4Z", "B7Z")))




testeff <- copy(filtre_fap(effectifs, "T4Z"))
ajout_libelle(testeff)




