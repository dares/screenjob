Spécialistes de la composition ou photocomposition, de l’impression, de la reliure ou du façonnage du papier-carton, les ouvriers des industries graphiques sont amenés à conduire des machines de plus en plus performantes ou à piloter des installations entièrement intégrées. Les innovations technologiques ont permis des gains de productivité importants dans ce secteur très concurrentiel, fragilisé notamment par la dématérialisation de nombreux imprimés ou les difficultés de la presse écrite. Ainsi le nombre d’emplois dans ces métiers n’a cessé de diminuer depuis le début des années 1990 et atteint 56 000 en moyenne sur la période 2012-2014.La population en emploi vieillit : la part des jeunes de moins de 30 ans a été divisée par trois en trente ans et ne représente aujourd’hui que 10 % des effectifs, tandis que la part des personnes âgées de 50 ans ou plus a doublé et représente 32 % des effectifs en 2012-2014. Près des trois quarts des emplois sont occupés par des hommes, comme au début des années 1980.Le niveau de formation des personnes en exercice s’est nettement accru depuis trente ans et la part des non diplômés, supérieure à 60 % au début des années 1980, s’établit désormais à 26 %. La proportion de bacheliers et de diplômés du supérieur a augmenté, mais les diplômes professionnels courts restent les plus fréquents. Les salaires sont dispersés dans ce métier. Un tiers des salariés à temps complet perçoit entre 1 500 et 2 000 € nets par mois.Le travail de nuit est fréquent, il concerne un tiers des ouvriers qualifiés des industries graphiques. Ces ouvriers travaillent généralement à temps plein, comme la plupart des ouvriers, et disposent le plus souvent d’un contrat ou d’un emploi à durée indéterminée. En majorité salariés, certains exercent aussi à leur compte en tant qu’imprimeur (16 % en 2012-2014). Si les salariés sur contrat à durée déterminée sont très minoritaires dans la profession, 84 % des embauches ont pourtant été faites sur ce type de contrat en 2014. Globalement, bien que la mobilité reste faible sur ce métier, elle a progressé ces dernières années.Le nombre des demandeurs d’emploi à la recherche d’un poste d’ouvrier des industries graphiques a légèrement augmenté au cours des dernières années. En 2014, près de la moitié des inscrits à Pôle emploi le sont depuis plus d’un an, contre 38 % pour l’ensemble des métiers.
